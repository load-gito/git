commitGraph.readChangedPaths::
	If true, then git will use the changed-path Bloom filters in the
	commit-graph file (if it exists, and they are present). Defaults to
	true. See linkgit:git-commit-graph[1] for more information.
